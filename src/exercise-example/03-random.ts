import {interval, Subject} from 'rxjs';
import { take, map } from 'rxjs/operators';
/**
 * exercise: Realize that the final two observables,
 * emit exactly the same value
 *
 * Tip: Hot Observable? subjects?
 */

(() => {

    // =======Not Touch this block===============
    const clock$ = interval(1000).pipe(
        take(5),
        map( val => Math.round(Math.random() * 100) )
    );
    // Not Touch the creation observable
    // ============================================

    const subject$ = new Subject();
    clock$.subscribe(subject$);


    // These two observables must emit exactly the same values
    subject$.subscribe( val => console.log('obs1', val) );
    subject$.subscribe( val => console.log('obs2', val) );
})();
