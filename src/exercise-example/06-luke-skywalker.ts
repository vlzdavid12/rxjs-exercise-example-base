import {ajax} from 'rxjs/ajax';
import {zip, of} from 'rxjs';
import {switchMap, map, tap} from 'rxjs/operators';


/**
 * Exercise:
 *  Make 2 HTTP (ajax) requests one time after another..
 *
 *  The first one must obtain the Star Wars character:
 *   Luke Skywalker, calling the endpoint:   /people/1/
 *
 *  The second request must be using the object
 *  from the previous request, and take the species (species),
 *  which is an array of URLs (array), within that array,
 *  take the first position and make the call to that URL,
 *  which should bring information about your species (Human)
 */

// Answer expect:
// Information about the humans in the universe of Start Wars
// Example of data expected
/**======
  { name: "Human",
  classification: "mammal",
  designation: "sentient",
  average_height: "180",
  skin_colors: "caucasian, black, asian, hispanic",
   …}
======**/

// Expected response with Greater difficulty
// Return the next object with the information of both requests
// Remembering that they shoot one after the other,
// with the URL that comes inside the array of 'species'

// Tip: research about the zip function:
//     That allows combining observables in an array of values
// https://rxjs-dev.firebaseapp.com/api/index/function/zip

// Example data expect:
/**=====
 films: {name: "Human",
    classification: "mammal",
    designation: "sentient",
    average_height: "180",
    skin_colors: "caucasian, black, asian, hispanic",
    // tslint:disable-next-line:jsdoc-format
    …}
 people: {name: "Luke Skywalker",
    height: "172",
    mass: "77",
    hair_color: "blond",
    skin_color: "fair",
    …}
 =====**/


(() => {

    // Do Not Touch ========================================================
    const SW_API = 'https://swapi.dev/api';
    const getRequest = (url: string) => ajax.getJSON<any>(url);
    // ==================================================================

    // Call the URL to get Luke Skywalker
    getRequest( `${SW_API}/people/3/` ).pipe(
        // Perform the respective operators here
        // First Response
        // switchMap(resp => getRequest(resp.species[0]))
        switchMap(resp => zip(of(resp), getRequest(resp.species[0]))),
        tap(console.log),
        map(([people, films]) => ({people, films}))
        // DO NOT TOUCH the subscribe or modify it ==
    ).subscribe(console.log);           // ==
    // =======================================


})();
