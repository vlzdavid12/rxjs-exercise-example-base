import {combineLatest, interval, timer} from 'rxjs';
import {map, take} from 'rxjs/operators';
/**
 * Exercise: Combine both observables (letras$, numeros$)
 * so that the emissions are the concatenation of the last
 * values emits
 */

//  Example output:
// a1
// a2
// b2
// b3
// c3
// c4
// d4
// d5
// e5

(() => {

    const letters = ['a', 'b', 'c', 'd', 'e'];
    const numbers = [1, 2, 3, 4, 5];

    // Emit letters every seconds
    const letters$ = interval(1000).pipe(
        map(i => letters[i]),
        take(letters.length)
    );

    // Emit numbers of 1 the 5 every seconds, for have in delay start
    // from 500 mil
    const numbers$ = timer(500, 1000).pipe(
        map(i => numbers[i]),
        take(numbers.length)
    );

    // ========================================
    // Start coding down here
    // Note, the subscribe must be like this
    // .subscribe( console.log )
    // That is, the output in the subscribe must
    // to be fully processed
    // ========================================

    combineLatest({
            letter: letters$,
            num: numbers$
        })
        .pipe(map<{
            letter: string;
            num: number;
        }, string>(({letter, num}) => letter + num)  )
        .subscribe(console.log);
})();
