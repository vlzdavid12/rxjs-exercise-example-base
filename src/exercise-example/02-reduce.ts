import {from} from 'rxjs';
import {filter, tap, reduce} from 'rxjs/operators';
/**
 * Exercise:
 * Sum the all numbers of array using reduce
 * Should of filter for so that only numbers be process
 * Output so that of 32
 * Tip:
 * isNan() is one function of javascript for determinate if the number is true
 * Use filter<any>(...) for have  not problem in type.
 */

(() => {

    const data: (string | number)[] = [1, 2, 'foo', 3, 5, 6, 'bar', 7, 8];

    const totalReduce = (accumulator: number, valueTotal: number) => {
        return accumulator + valueTotal;
    };

    from(data).pipe(
        // Working her
        filter((val: any) => !isNaN(val)),
        reduce<number , number>(totalReduce)
    ).subscribe((resp: number) => console.log(resp)); // Result Output number 32


})();
