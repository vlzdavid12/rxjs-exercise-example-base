/**
 * Exercise:
 * The objective of is to perform the same operation,but using in observables
 * Nota: Not using the circle "FOR OF", using a observable and call function capitalize
 */

/**
 * Output expect:
 * Batman
 * Joker
 * Doble Cara
 * Pingüino
 * Hiedra Venenosa
 */
import {from} from 'rxjs';
import {map} from 'rxjs/operators';
(() => {



    const names = ['batman', 'joker', 'doble cara', 'pingüino', 'hiedra venenosa'];

    const capitalize = (name: string) => name.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());

    // This Change FOR OF, for in Observable and capitalize the emissions
    // for (const name of names) {
    //    console.log(capitalize(name));
    // }

    from(names)
        .pipe(map(capitalize))
        .subscribe(console.log);
})();
