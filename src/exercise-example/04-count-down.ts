import {interval} from 'rxjs';
import {map, take, tap} from 'rxjs/operators';
/**
 * Exercise: Count Down
 * start of number 7
 */

// Output , expect ===
// 7
// 6
// 5
// 4
// 3
// 2
// 1
// 0

(() => {

    const start = 7;
    const countdown$ = interval(700).pipe(
        // Use the necessary operators
        // to count down
        map(i => start - i),
        take(start + 1)
    );

    // Not touch line ==================
    countdown$.subscribe(console.log); // =
    // ======================================

})();
