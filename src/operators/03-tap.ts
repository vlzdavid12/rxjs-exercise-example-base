import {range} from 'rxjs';
import {tap, map} from 'rxjs/operators';

const number$ = range(1, 5)
    .pipe(
        map((val: number) => (val + 10).toString()),
        tap(console.log));
number$.subscribe(val => console.log('Subs', val));

const number2$ = range(1, 20)
    .pipe(
        map((val: number) => (val + 2).toString()),
        tap({
            next: value => console.log(value),
            complete: () => console.log('Tap Complete')
        }));
number2$.subscribe(val => console.log('Subs', val));
