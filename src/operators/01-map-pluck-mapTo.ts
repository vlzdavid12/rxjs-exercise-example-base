import {range, fromEvent, pluck, tap, mapTo} from 'rxjs';
import {map} from 'rxjs/operators';

// MAP

range(1, 5)
    .pipe(map(val => (val * 10 ).toString()))
    .subscribe(console.log);


const keyUp$ =  fromEvent<KeyboardEvent>(document, 'keyup');

const keyupCode$ = keyUp$
    .pipe(map<KeyboardEvent, string>(event => event.code));

keyupCode$.subscribe(resp => console.log('Map: ', resp));


// PLUCK

const keyupPluck$ = keyUp$
    .pipe(pluck<KeyboardEvent>('target', 'baseURI'));

keyupPluck$.subscribe( resp => console.log('Pluck: ', resp));


// MAP TO

const keyupMapTo$ = keyUp$
    .pipe(mapTo<KeyboardEvent, string>('Key Press'));

keyupMapTo$.subscribe(resp => console.log('MapTo', resp));




