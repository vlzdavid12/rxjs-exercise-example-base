import {range, from, of, fromEvent} from 'rxjs';
import {filter, map} from 'rxjs/operators';

interface Persons {
    type: string;
    name: string;
}


const observable$ = range(1, 10);
observable$
    .pipe(filter<number>((value: number, i: number) => {
        console.log('Index: ', i);
        return value % 2 === 1;
    }))
    .subscribe(console.log);

const persons: Persons[]  = [
    {
        type: 'hero',
        name: 'Batman'
    },
    {
        type: 'villano',
        name: 'Shocker'
    },
    {
        type: 'hero',
        name: 'Thor'
    }
];

of(...persons)
    .pipe(filter((person: Persons) => person.type === 'villano'))
    .subscribe(resp => console.log('Of', resp));

from(persons)
    .pipe(filter((person: Persons) => person.type === 'hero'))
    .subscribe(resp => console.log('from', resp));

const keyUp$ = fromEvent<KeyboardEvent>(document, 'keyup')
    .pipe(
        map((val: KeyboardEvent) => val.code),
        filter((val: string) => val === 'Enter')
    );

keyUp$.subscribe(console.log);

