import {from} from 'rxjs';
import {scan, reduce, map} from 'rxjs/operators';

const numbers: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const totalReduce =  (acc: number, cur: number) => acc + cur;

from(numbers)
    .pipe(scan(totalReduce))
    .subscribe(resp => console.log('SCAN: ', resp));

from(numbers)
    .pipe(reduce(totalReduce))
    .subscribe(resp => console.log('REDUCE: ', resp));


// Redux

interface User {
    id?: string;
    authenticate?: boolean;
    token?: string;
    age?: number;
}

const user: User[] = [
    { id: 'fher', authenticate: true, token: ''},
    { id: 'fher', authenticate: true, token: ''},
    { id: 'fher', authenticate: true, token: ''}
];



const state$ = from(user).pipe(
    scan<User, any>((arc, cur) => {
        return {...arc, ...cur};
    }, {age: 23})
);

const id$ = state$.pipe(
    map(state => state.id)
);

id$.subscribe(console.log)

