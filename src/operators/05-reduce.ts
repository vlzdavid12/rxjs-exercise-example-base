import {interval, take} from 'rxjs';
import {reduce} from 'rxjs/operators';

const numbers: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const totalReduce = (accumulator: number, valueTotal: number) => {
    return accumulator + valueTotal;
};

const total = numbers.reduce(totalReduce);

console.log('Total Reduce JS: ', total);


interval(1000).pipe(
    take(5),
    reduce(totalReduce, 20)
).subscribe(
    {
        next: val => console.log(val),
        complete: () => console.log('Process complete')
    }
);
