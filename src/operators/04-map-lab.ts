import {fromEvent} from 'rxjs';
import {map, tap} from 'rxjs/operators';

// Function calculate
const calculateScrollEvent = (event: any) => {
    const {scrollTop, scrollHeight, clientHeight} = event.target.documentElement;

    return (scrollTop / (scrollHeight - clientHeight)) * 100;
};


const text = document.createElement('div');
text.innerHTML = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br>
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,  <br>
when an unknown printer took a galley of type and scrambled it to make a type  <br>
specimen book. It has survived not only five centuries, but also the leap into  <br>
electronic typesetting, remaining essentially unchanged. It was popularised in  <br>
the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,  <br>
and more recently with desktop publishing software like Aldus PageMaker including  <br>
versions of Lorem Ipsum.

Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br>
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,  <br>
when an unknown printer took a galley of type and scrambled it to make a type  <br>
specimen book. It has survived not only five centuries, but also the leap into  <br>
electronic typesetting, remaining essentially unchanged. It was popularised in  <br>
the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,  <br>
and more recently with desktop publishing software like Aldus PageMaker including  <br>
versions of Lorem Ipsum.
<br><br>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br>
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,  <br>
when an unknown printer took a galley of type and scrambled it to make a type  <br>
specimen book. It has survived not only five centuries, but also the leap into  <br>
electronic typesetting, remaining essentially unchanged. It was popularised in  <br>
the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,  <br>
and more recently with desktop publishing software like Aldus PageMaker including  <br>
versions of Lorem Ipsum.
<br><br>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br>
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,  <br>
when an unknown printer took a galley of type and scrambled it to make a type  <br>
specimen book. It has survived not only five centuries, but also the leap into  <br>
electronic typesetting, remaining essentially unchanged. It was popularised in  <br>
the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,  <br>
and more recently with desktop publishing software like Aldus PageMaker including  <br>
versions of Lorem Ipsum.
<br><br>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br>
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,  <br>
when an unknown printer took a galley of type and scrambled it to make a type  <br>
specimen book. It has survived not only five centuries, but also the leap into  <br>
electronic typesetting, remaining essentially unchanged. It was popularised in  <br>
the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,  <br>
and more recently with desktop publishing software like Aldus PageMaker including  <br>
versions of Lorem Ipsum.
<br><br>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br>
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,  <br>
when an unknown printer took a galley of type and scrambled it to make a type  <br>
specimen book. It has survived not only five centuries, but also the leap into  <br>
electronic typesetting, remaining essentially unchanged. It was popularised in  <br>
the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,  <br>
and more recently with desktop publishing software like Aldus PageMaker including  <br>
versions of Lorem Ipsum.`;


const body = document.querySelector('body');
body?.append(text);


const progressBar = document.createElement('div');
progressBar.setAttribute('class', 'progress-bar');

body?.append(progressBar);



// Streams
const scroll$ = fromEvent(document, 'scroll');
// scroll$.subscribe(console.log);

const progress$ = scroll$.pipe(
    map((event) => calculateScrollEvent(event))
);

progress$.subscribe(percentage => {
    console.log(percentage);
    progressBar.style.width = `${percentage}%`;
});

