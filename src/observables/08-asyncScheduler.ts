import {asyncScheduler} from 'rxjs';

const hello = ( ) => console.log('Hello world');
const hello2 = (name: any) => console.log(`Hello ${name}`);

/*asyncScheduler.schedule(hello, 2000);
asyncScheduler.schedule(hello2, 2000, 'David');*/


const subs = asyncScheduler.schedule(function(state: any): void{
    console.log('state', state);
    this.schedule(state + 1, 1000);
}, 3000, 0);

asyncScheduler.schedule(() => {
    console.log('Unsubscribe');
    subs.unsubscribe();
}, 6000);
