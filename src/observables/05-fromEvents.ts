import {fromEvent} from 'rxjs';

const src1$ = fromEvent<MouseEvent>(document, 'click');
const src2$ = fromEvent<KeyboardEvent>(document, 'keyup');

const observer =  {
    next: (value: any) => console.log('Next:', value)
};

// src1$.subscribe(observer);

src1$.subscribe(({x, y}): void => {
    console.log('EjeX: ' + x, 'EjeY:' + y);
});

src2$.subscribe(event => {
    console.log(event);
});


