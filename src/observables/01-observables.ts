import {Observable, Observer} from 'rxjs';

const observer: Observer<any> = {
    next: value =>  console.log('Next: ', value),
    error: error => console.log('Error: ', error),
    complete: () => console.log('Complete')
};


const obs$ = new Observable<string>(subs => {
    subs.next('Hey');
    subs.next('What It is');
    subs.complete();
    subs.next('Next complete not process');
});

obs$.subscribe(observer);
