import {from, of} from 'rxjs';

const observer = {
    next: (val: any) => console.log('Next: ', val),
    complete: () => console.log('Complete')
};


// const source$ = from<number[]>([1, 2, 3, 4, 5, 6]);
// const source2$ = of<number[]>(...[1, 2, 3, 4, 5, 6]);

// source$.subscribe(observer);


// tslint:disable-next-line:typedef
/*const miGenerator = function*() {
    yield 1;
    yield 2;
    yield 3;
    yield 4;
    yield 5;
};*/

// const myIterable = miGenerator();

// from(myIterable).subscribe(observer);

const source$ = from(fetch('https://api.github.com/users/klerith'));
source$.subscribe(async (resp) => {
    console.log(resp);
    const dataResp = await resp.json();
    console.log(dataResp);
});








