import {of} from 'rxjs';

const obs$ = of<number[]>(...[1, 2, 3, 4, 5]);

console.log('Star of Obs$');
obs$.subscribe(
    next => console.log(next),
    (error) => console.log(error),
    () => console.log('Complete')
);
console.log('Find of Obs$');
