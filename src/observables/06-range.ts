import {asyncScheduler, range} from 'rxjs';

const src$ = range(1, 9, asyncScheduler);
console.log('Star Range');
src$.subscribe(console.log);
console.log('End Range');
