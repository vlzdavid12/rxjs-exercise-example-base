import {Observable, Observer, Subject} from 'rxjs';

const observer: Observer<any> = {
    next: value => console.log('Next: ', value),
    error: error => console.warn('Error: ', error),
    complete: () => console.log('Complete')
}

const interval$ = new Observable(subs => {
    const intervalID = setInterval(
        () => subs.next(Math.random()), 1000
    );

    return () => {
        clearInterval(intervalID);
        console.log('Interval destroy success fully');
    }
});

const subject$ = new Subject();
const subscription = interval$.subscribe(subject$);


const subs1 = subject$.subscribe(observer);

setTimeout(() => {
    subject$.next(30),
        subject$.complete(),
        subscription.unsubscribe();
}, 4000)

