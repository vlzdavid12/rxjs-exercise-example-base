import {interval, timer} from 'rxjs';


const observer =  {
    next: (val: number) =>  console.log(val),
    complete: () =>  console.log('Process Complete!')
};

const dayIn5 = new Date();
dayIn5.setSeconds(dayIn5.getSeconds() + 5);

const interval$ = interval(1000);

const timer$ = timer(dayIn5);

console.log('Start');
interval$.subscribe(observer);
console.log('End');
