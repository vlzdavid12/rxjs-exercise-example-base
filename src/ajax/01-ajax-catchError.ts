import {ajax, AjaxError} from 'rxjs/ajax';
import {pluck, catchError} from 'rxjs/operators';
import {of} from 'rxjs';


const url = 'https://api.github.com/user?per_page=5';


const getError = (err: AjaxError) => {
    console.warn('Error in: ' , err);
    return of([]);
};


ajax(url).pipe(
    pluck('response'),
    catchError(getError)
).subscribe(users =>  console.log('Users: ', users))
