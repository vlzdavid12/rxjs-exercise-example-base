import {ajax} from 'rxjs/ajax';

const url = 'https://httpbin.org/delay/1';

ajax.put(url, {
    id: 1,
    name: 'david'
}, {
    'mi-token': 'adshadshj748934'
}).subscribe(console.log);


ajax({
    url,
    method: 'DELETE',
    headers: {
        'mi-token': 'adshadshj748934'
    },
    body: {
        id: 1,
        name: 'david'
    }
}).subscribe(console.log);
