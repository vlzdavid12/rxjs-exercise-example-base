import {ajax, AjaxError} from 'rxjs/ajax';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

const url = 'https://httpbin21.org/delay/1';

const getError = (resp: AjaxError) => {
    console.warn('Error: ', resp.message);
    return of({
        ok: false,
        users: [],
    });
};


/*const obs$ = ajax.
getJSON(url)
    .pipe(catchError(getError));

const obs2$ = ajax.get(url)
    .pipe(catchError(getError));*/

const obs$ = ajax.getJSON(url);
const obs2$ = ajax.get(url);

obs$.pipe(catchError(getError))
    .subscribe({
    next: (data: any) => console.log('getJson: ', data),
    error: (resp) => console.warn(resp),
    complete: () => console.log('Process getJson terminate')
});

obs2$.subscribe({
    next: (data: any) => console.log('ajax: ', data),
    error: (resp) => console.warn(resp),
    complete: () => console.log('Process ajax terminate')
});
