import {fromEvent, pluck, of, Observable} from 'rxjs';
import {tap, map, catchError, switchMap, exhaustMap} from 'rxjs/operators';
import {ajax} from 'rxjs/ajax';
const form = document.createElement('form');
const inputEmail = document.createElement('input');
const inputPass = document.createElement('input');
const submitBtn = document.createElement('button');

const getHttpLogin =  (userPass: {email: string, password: string}) => {
    return ajax.post('https://reqres.in/api/login?delay=1', userPass).pipe(
        pluck('response', 'token'),
        catchError(err => of(err))
    );
};


form.classList.add('form-login');
inputEmail.classList.add('input-form');
inputPass.classList.add('input-form');
submitBtn.classList.add('btn-form');


// Configuration
inputEmail.type = 'email';
inputEmail.placeholder = 'Insert email';
inputEmail.value =  'eve.holt@reqres.in';

inputPass.type = 'password';
inputPass.placeholder = 'Insert password';
inputPass.value =  'cityslicka';

submitBtn.innerHTML = 'start';

form.append(inputEmail, inputPass, submitBtn);
document.querySelector('body')?.append(form);

const submitForm$ =  fromEvent<Event>(form, 'submit')
    .pipe(
        tap(ev => ev.preventDefault()),
        map((ev: any) => ({
            email: ev.target[0].value,
            password: ev.target[1].value
        })),
        exhaustMap(getHttpLogin)
    );

submitForm$.subscribe(token => console.log(token));



