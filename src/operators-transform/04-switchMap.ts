import {fromEvent,  Observable} from 'rxjs';
import {map, debounceTime, pluck, switchMap} from 'rxjs/operators';
import {GithubUsersRepo} from '../interfaces/gitjhup-users.interface';

import {ajax} from 'rxjs/ajax';

const url = `https://api.github.com/`;

// Reference
const body = document.querySelector('body');
const textInput = document.createElement('input');
const orderList = document.createElement('ol');

textInput?.classList.add('text');
body?.append(textInput, orderList);

// Helpers
const showUsers = (users: GithubUsersRepo[]): void => {
    orderList.innerHTML = '';

    for (const user of users) {

        const li = document.createElement('li');
        const img = document.createElement('img');
        const title = document.createElement('div');
        title.classList.add('parrafo');
        img.src = user.avatar_url;
        title.textContent = user.login;


        const witch = document.createElement('a');
        witch.href = user.html_url;
        witch.text = 'Show Page';
        witch.target = '_blank';

        li.append(img);
        li.append(title);
        li.append(witch);


        orderList.append(li);

    }

};

// Streams

const input$ = fromEvent<KeyboardEvent>(textInput, 'keyup');

input$.pipe(
    debounceTime<KeyboardEvent>(500),
    pluck<KeyboardEvent>('target', 'value'),
    switchMap<unknown, Observable<GithubUsersRepo>>(txt => ajax.getJSON(`${url}search/users?q=${txt}`)),
    pluck<GithubUsersRepo>('items')
); // .subscribe((resp: any) => showUsers(resp));



const url2 = 'https://httpbin.org//delay/1?arg=';

input$.pipe(
    pluck('target', 'value'),
    switchMap(txt => ajax.getJSON(url2 + txt))
).subscribe(console.log);

