import {fromEvent, interval, Observable, of} from 'rxjs';
import {mergeMap, take, map, takeUntil} from 'rxjs/operators';
const letters$: Observable<string> = of('a', 'b', 'c', 'd');

letters$
    .pipe(
        mergeMap((letter) => interval(1000).pipe(
            map(i => letter + i),
            take(3)
        )),

    );

    /*.subscribe({
    next: val => console.log('Next: ', val),
    complete: () => console.log('Process merMap Complete...')
});*/


const mouseDown$ = fromEvent(document, 'mousedown');
const mouseUp$ = fromEvent(document, 'mouseup');
const interval$ = interval();

mouseDown$
    .pipe(mergeMap(() => interval$.pipe(
        takeUntil(mouseUp$)
    )))
    .subscribe(console.log);



