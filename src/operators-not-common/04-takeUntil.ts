import {fromEvent, interval} from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const button = document.createElement('button');
button.classList.add('button');
button.innerHTML =  'Stop Time';

const body = document.querySelector('body');
body?.append(button);

const  counter$ = interval(1000);
const clickBtn$ = fromEvent(button, 'click');

counter$
    .pipe(takeUntil(clickBtn$))
    .subscribe({
    next: val => console.log('Count: ', val),
    complete: () => console.log('Process takeUntil complete...')
});
