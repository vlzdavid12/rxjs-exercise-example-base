import {Observable, of} from 'rxjs';
import {take} from 'rxjs/operators';

const number$: Observable<number> = of(1, 2, 3, 4, 5, 6, 7, 8, 9);

number$.pipe(take(3))
    .subscribe({
        next: value => console.log('TAKE: ', value),
        complete: () => console.log('Complete process take.')
    });
