import {Observable, of, from} from 'rxjs';
import {distinct} from 'rxjs/operators';

const numbers$: Observable<number | string> = of(1, 2, 3, 4, '4', 2, 5, 5, 6, 7, 8, 9, 1);

numbers$
    .pipe(
        distinct() // ===
    )
    .subscribe({
        next: val => console.log(val),
        complete: () => console.log('Process distinct complete')
    });


interface Person {
    name: string;
}

const persons: Person[] = [
    {name: 'Megaman'},
    {name: 'Capitan America'},
    {name: 'Super Man'},
    {name: 'Thor'},
    {name: 'Super Man'},
    {name: 'Batman'},
    {name: 'Flash'},
    {name: 'Super Woman'},
    {name: 'Super Man'},
];

const persons$: Observable<Person>  =  from(persons);

persons$
    .pipe(
        distinct(p => p.name)
    )
    .subscribe({
        next: val => console.log(val),
        complete: () => console.log('Process distinct complete')
    });
