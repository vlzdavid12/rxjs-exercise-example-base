import { fromEvent } from 'rxjs';
import { first } from 'rxjs/operators';

const click$ = fromEvent<MouseEvent>(document, 'click');

click$
    .pipe(first<MouseEvent>(event => event.clientY >= 150))
    .subscribe({
    next: val => console.log('Next:', val),
    complete: () => console.log('Complet process first')
});
