import {fromEvent, takeWhile} from 'rxjs';
import {map} from 'rxjs/operators';


const  click$ = fromEvent<MouseEvent>(document, 'click');

click$
    .pipe(
        map(({x, y}) => ({x, y})),

        takeWhile(({y}) => y <= 200, true)
        )
    .subscribe({
    next: value => console.log(value),
    complete: () =>  console.log('Click Complete')
});
