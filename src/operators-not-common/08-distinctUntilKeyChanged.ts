import {from, Observable} from 'rxjs';
import { distinctUntilKeyChanged } from 'rxjs/operators';


interface Person {
    name: string;
}

const persons: Person[] = [
    {name: 'Megaman'},
    {name: 'Megaman'},
    {name: 'Capitan America'},
    {name: 'Super Man'},
    {name: 'Thor'},
    {name: 'Super Man'},
    {name: 'Batman'},
    {name: 'Batman'},
    {name: 'Flash'},
    {name: 'Super Woman'},
    {name: 'Super Man'},
];

const persons$: Observable<Person>  =  from(persons);

persons$
    .pipe(
        distinctUntilKeyChanged('name')
    )
    .subscribe({
        next: val => console.log(val),
        complete: () => console.log('Process distinct complete')
    });
