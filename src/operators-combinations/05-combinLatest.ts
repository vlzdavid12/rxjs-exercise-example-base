import {fromEvent, combineLatest} from 'rxjs';
import {pluck} from 'rxjs/operators';

const keyUp$ = fromEvent(document, 'keyup');
const click$ = fromEvent(document, 'click');

/*combineLatest(
    keyUp$.pipe(pluck('type')),
    click$.pipe(pluck('type'))
).subscribe(console.log);*/

const div = document.createElement('div');
const input1 = document.createElement('input');
const input2 = document.createElement('input');

div.classList.add('form-login');
input1.classList.add('input-form');
input2.classList.add('input-form');


input1.placeholder =  'email@gmail.com';

input2.placeholder =  '*****';
input2.type = 'password';

div.append(input1, input2);

document.querySelector('body')?.append( div);

const getInputStream = (elem: HTMLElement) => {
    return fromEvent<KeyboardEvent>(elem, 'keyup').pipe(
        pluck<KeyboardEvent>('target', 'value')
    );
};

combineLatest(
    getInputStream(input1),
    getInputStream(input2)
).subscribe(console.log);
