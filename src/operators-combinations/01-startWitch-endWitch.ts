import {of} from 'rxjs';
import {startWith, endWith} from 'rxjs';

const numbers$ = of(1, 2, 3, 4, 5, 6, 7, 8, 9)
    .pipe(
        startWith('a', 'b', 'c', 'd'),
        endWith('w', 'x', 'y', 'z')
        );

numbers$.subscribe(console.log);
