import {of, interval, forkJoin} from 'rxjs';
import {take} from 'rxjs/operators';

const numbers$ = of(1, 2, 3, 4, 5, 6, 7, 8, 9);
const interval$ = interval(1000)
    .pipe(take(3));
const letters$ = of('a', 'b', 'c', 'd', 'e', 'f' , 'g');

forkJoin(
    numbers$,
    interval$,
    letters$
).subscribe(console.log);


forkJoin(
    numbers$,
    interval$,
    letters$
).subscribe(resp => {
    console.log('numbers:' , resp[0]);
    console.log('interval:' , resp[1]);
    console.log('letters:' , resp[2]);
});


forkJoin({
    num: numbers$,
    int: interval$,
    let: letters$
}).subscribe(resp => console.log(resp));
