import {GithubUsersRepo} from './gitjhup-users.interface';

export interface GithubUserRepo {
    total_count: number;
    incomplete_results: boolean;
    items: GithubUsersRepo[];
}
